<?php




class RememberTokens extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $id;
     
    /**
     *
     * @var integer
     */
    public $user_id;
     
    /**
     *
     * @var string
     */
    public $token;
     
    /**
     *
     * @var string
     */
    public $user_agent;
     
    /**
     *
     * @var string
     */
    public $created_at;
     
    /**
     * Initialize method for model.
     */
    public function initialize()
    {
		$this->setSource('Remember_Tokens');

    }

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'id' => 'id', 
            'user_id' => 'user_id', 
            'token' => 'token', 
            'user_agent' => 'user_agent', 
            'created_at' => 'created_at'
        );
    }

}
