<?php




class Tasks extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $id;
     
    /**
     *
     * @var integer
     */
    public $project_id;
     /**
     *
     * @var integer
     */
    public $order;
     
    /**
     *
     * @var string
     */
    public $title;
     
    /**
     *
     * @var string
     */
    public $description;
     
    /**
     *
     * @var integer
     */
    public $user_id;
     
    /**
     *
     * @var integer
     */
    public $assign_to;
     
    /**
     *
     * @var string
     */
    public $stage;
     
    /**
     *
     * @var string
     */
    public $priority;
     
    /**
     *
     * @var string
     */
    public $created_at;
     
    /**
     *
     * @var string
     */
    public $updated_at;
     
    /**
     * Independent Column Mapping.
     */
    public function beforeValidationOnCreate() {
        $this->stage = "todo";
        $this->order = 0;
        $this->updated_at = date('Y-m-d H:i:s');
        $this->created_at = date('Y-m-d H:i:s');
   }
   
   public function initialize() {
        $this->belongsTo('assign_to', 'Users', 'id', array(
            'alias' => 'user',
            'reusable' => true
        ));
    }
    public function columnMap()
    {
        return array(
            'id' => 'id', 
            'project_id' => 'project_id', 
            'order' => 'order', 
            'title' => 'title', 
            'description' => 'description', 
            'user_id' => 'user_id', 
            'assign_to' => 'assign_to', 
            'stage' => 'stage', 
            'priority' => 'priority', 
            'created_at' => 'created_at', 
            'updated_at' => 'updated_at'
        );
    }

}
