<?php




class Projects extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $id;
     
    /**
     *
     * @var string
     */
    public $project_title;
     
    /**
     *
     * @var integer
     */
    public $user_id;
     
    /**
     *
     * @var string
     */
    public $project_description;
     
    /**
     *
     * @var string
     */
    public $created_at;
     
    /**
     *
     * @var string
     */
    public $updated_at;
     
    /**
     * Independent Column Mapping.
     */
    
    public function beforeValidationOnCreate() {

        $this->updated_at = date('Y-m-d H:i:s');
        $this->created_at = date('Y-m-d H:i:s');
   }
   /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->hasMany("id", "Tasks", "project_id", 
                     array('alias' => 'tasks'));
    }
    public function columnMap()
    {
        return array(
            'id' => 'id', 
            'project_title' => 'project_title', 
            'user_id' => 'user_id', 
            'project_description' => 'project_description', 
            'created_at' => 'created_at', 
            'updated_at' => 'updated_at'
        );
    }

}
