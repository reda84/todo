<?php




class Accounts extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $id;
     
    /**
     *
     * @var integer
     */
    public $admin_id;
     
    /**
     *
     * @var string
     */
    public $account_name;
     
    /**
     *
     * @var string
     */
    public $created_at;
     
    /**
     *
     * @var string
     */
    public $updated_at;
     
    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'id' => 'id', 
            'admin_id' => 'admin_id', 
            'account_name' => 'account_name', 
            'created_at' => 'created_at', 
            'updated_at' => 'updated_at'
        );
    }

}
