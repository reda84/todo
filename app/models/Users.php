<?php

use Phalcon\Mvc\Model\Validator\Email as Email;
use Phalcon\Mvc\Model\Validator\Uniqueness;

class Users extends \Phalcon\Mvc\Model {

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var integer
     */
    public $account_id;

    /**
     *
     * @var integer
     */
    public $profile_id;

    /**
     *
     * @var integer
     */
    public $avatar_id;

    /**
     *
     * @var string
     */
    public $username;

    /**
     *
     * @var string
     */
    public $email;

    /**
     *
     * @var string
     */
    public $password;

    /**
     *
     * @var string
     */
    public $created_at;

    /**
     *
     * @var string
     */
    public $udated_at;

    /**
     * Validations and business logic
     */
    public function validation() {

        $this->validate(
                new Email(
                array(
            "field" => "email",
            "required" => true,
                )
                )
        );
        $this->validate(new Uniqueness(array(
            "field" => "email",
            "message" => "The email is already registered"
        )));
        if ($this->validationHasFailed() == true) {
            return false;
        }
    }

    public function initialize() {
        $this->belongsTo('profile_id', 'Profiles', 'id', array(
            'alias' => 'profile',
            'reusable' => true
        ));
        $this->belongsTo('account_id', 'Accounts', 'id', array(
            'alias' => 'account',
            'reusable' => true
        ));
        $this->hasOne('avatar_id', 'Media', 'id', array(
            'alias' => 'avatar',
            'reusable' => true
        ));
    }

    public function beforeValidationOnCreate() {
        $this->profile_id = 1;
        $this->avatar_id = 0;
        $this->account_id = 0;
        $this->updated_at = date('Y-m-d H:i:s');
        $this->created_at = date('Y-m-d H:i:s');
    }

    public function afterCreate() {
        $account = new Accounts();
        $account->account_name = $this->username;
        $account->admin_id = $this->id;
        $account->updated_at = date('Y-m-d H:i:s');
        $account->created_at = date('Y-m-d H:i:s');
        $this->account = $account;
        $this->save();
    }
    
    public function getAvatar(){
        if($this->avatar){
           return  "/uploads/users/" . $this->avatar->name ;
        }else{
            return "/uploads/users/default.jpg";
        }
    }

    /**
     * Independent Column Mapping.
     */
    public function columnMap() {
        return array(
            'id' => 'id',
            'account_id' => 'account_id',
            'profile_id' => 'profile_id',
            'username' => 'username',
            'email' => 'email',
            'avatar_id' => 'avatar_id',
            'password' => 'password',
            'created_at' => 'created_at',
            'updated_at' => 'updated_at'
        );
    }

}
