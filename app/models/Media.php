<?php

class Media extends \Phalcon\Mvc\Model {

    public $upload_path;

    const RESOURCE_ID = 4;

    /* Database Fields */

    public $id;
    public $author_id;
    public $name;
    public $slug;
    public $ext;
    public $size;
    public $mime_type;
    public $updated_at;
    public $created_at;

    /**
     * Initialize method for model.
     */
    public function initialize() {
        $this->upload_base = "/uploads";
        $this->upload_path = __DIR__ . '/../../public' . $this->upload_base;
        $this->setSchema("");
        $this->addBehavior(new Sluggable(array(
            "slug" => "slug",
            "title" => "name",
        )));
    }

    /**
     * Before create the user assign a password
     */
    public function beforeValidationOnCreate() {
        $di = $this->getDI();
        $Auth = $di->get('auth');
        $identity = $Auth->getIdentity();
        $this->author_id = $identity["id"];
        $this->route_id = 0;
        $this->updated_at = date('Y-m-d H:i:s');
        $this->created_at = date('Y-m-d H:i:s');
    }

    /**
     * Independent Column Mapping.
     */
    public function columnMap() {
        return array(
            'id' => 'id',
            'author_id' => 'author_id',
            'name' => 'name',
            'slug' => 'slug',
            'ext' => 'ext',
            'size' => 'size',
            'mime_type' => 'mime_type',
            'updated_at' => 'updated_at',
            'created_at' => 'created_at'
        );
    }

    public function saveMedia($data) {
        
    }

    public function upload() {

        // HTTP headers for no cache etc
        header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");


        //fromat the upload dir
        $targetDir = $this->upload_path;
        // Create target dir
        if (!file_exists($targetDir))
            @mkdir($targetDir);
        $targetDir = $targetDir . "/" . date('Y');
        if (!is_dir($targetDir)) {
            @mkdir($targetDir);
        }
        $targetDir = $targetDir . "/" . date('m');
        if (!is_dir($targetDir)) {
            @mkdir($targetDir);
        }

        //$cleanupTargetDir = false; // Remove old files
        //$maxFileAge = 60 * 60; // Temp file age in seconds
        // 5 minutes execution time
        @set_time_limit(5 * 60);


        // Get parameters
        $chunk = isset($_REQUEST["chunk"]) ? $_REQUEST["chunk"] : 0;
        $chunks = isset($_REQUEST["chunks"]) ? $_REQUEST["chunks"] : 0;
        $fileName = isset($_REQUEST["name"]) ? $_REQUEST["name"] : '';

        // Clean the fileName for security reasons
        $fileName = preg_replace('/[^\w\._]+/', '', $fileName);

        $path_parts = pathinfo($fileName);
        $this->name = $path_parts['filename'];
        $this->ext = $path_parts['extension'];
        $this->slug = $this->generateSlug();
        $this->size = ($_FILES["file"]["size"] / 1024); //  the size in kb
        $fileName = $this->slug . '.' . $this->ext;

        $finfo = finfo_open(FILEINFO_MIME_TYPE);
        $this->mime_type = finfo_file($finfo, $_FILES['file']['tmp_name']);

//            // Make sure the fileName is unique but only if chunking is disabled
//            if ($chunks < 2 && file_exists($targetDir . DIRECTORY_SEPARATOR . $fileName)) {
//                 $ext = strrpos($fileName, '.');
//                 $fileName_a = substr($fileName, 0, $ext);
//                 $fileName_b = substr($fileName, $ext);
//
//                 $count = 1;
//                 while (file_exists($targetDir . DIRECTORY_SEPARATOR . $fileName_a . '_' . $count . $fileName_b))
//                      $count++;
//                 $fileName = $fileName_a . '_' . $count . $fileName_b;
//            }
// Look for the content type header
        if (isset($_SERVER["HTTP_CONTENT_TYPE"]))
            $contentType = $_SERVER["HTTP_CONTENT_TYPE"];

        if (isset($_SERVER["CONTENT_TYPE"]))
            $contentType = $_SERVER["CONTENT_TYPE"];

// Handle non multipart uploads older WebKit versions didn't support multipart in HTML5
        if (strpos($contentType, "multipart") !== false) {
            if (isset($_FILES['file']['tmp_name']) && is_uploaded_file($_FILES['file']['tmp_name'])) {
                // Open temp file
                $out = fopen($targetDir . DIRECTORY_SEPARATOR . $fileName, $chunk == 0 ? "wb" : "ab");
                if ($out) {
                    // Read binary input stream and append it to temp file
                    $in = fopen($_FILES['file']['tmp_name'], "rb");

                    if ($in) {
                        while ($buff = fread($in, 4096))
                            fwrite($out, $buff);
                    } else
                        die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');
                    fclose($in);
                    fclose($out);
                    @unlink($_FILES['file']['tmp_name']);
                } else
                    die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "Failed to open output stream."}, "id" : "id"}');
            } else
                die('{"jsonrpc" : "2.0", "error" : {"code": 103, "message": "Failed to move uploaded file."}, "id" : "id"}');
        } else {
            // Open temp file
            $out = fopen($targetDir . DIRECTORY_SEPARATOR . $fileName, $chunk == 0 ? "wb" : "ab");
            if ($out) {
                // Read binary input stream and append it to temp file
                $in = fopen("php://input", "rb");

                if ($in) {
                    while ($buff = fread($in, 4096))
                        fwrite($out, $buff);
                } else
                    die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');

                fclose($in);
                fclose($out);
            } else
                die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "Failed to open output stream."}, "id" : "id"}');
        }

        if ($this->save()) {
            echo json_encode(array(
                "file_id" => $this->id,
                "file_url" => $this->getPath(),
                "filename" => $fileName
            ));
        } else {
            echo json_encode($this);
        }
        // Return JSON-RPC response

        die();
    }

    public function deleteMedia($id) {
        $media = self::findFirst($id);
        $file = $this->getPath();
        if ($media->id) {
            if ($media->delete()) {
                unlink($file);
            }
        }
    }

    public function uploadUserAvater($request) {
        // Print the real file names and sizes
        foreach ($request->getUploadedFiles() as $file) {

            $fileName = $file->getName();
            $path_parts = pathinfo($fileName);
            $this->ext = $path_parts['extension'];
            $this->name = $path_parts['filename'];
            $this->slug = $this->generateSlug();
            $this->size = $file->getSize();
            $fileName = $this->slug . '.' . $this->ext;
            $this->name = $fileName;
            $this->mime_type = $file->getType();
            $file->moveTo(__DIR__ . '/../../public/uploads/users/' . $fileName);
            if ($this->save()) {
                echo json_encode(array(
                    "file_id" => $this->id,
                    "file_url" => $this->getUserAvatarPath(),
                    "filename" => $fileName
                ));
            } else {
                echo json_encode($this);
            }
        }
    }

    protected function getPath() {
        if ($this->id) {
            $y = date("Y", strtotime($this->created_at));
            $d = date("m", strtotime($this->created_at));
            $path = $this->upload_base . "/" . $y . "/" . $d . "/" . $this->slug . "." . $this->ext;
            return $path;
        }
    }

    protected function getUserAvatarPath() {
        return "/uploads/users/" . $this->name;
    }

}
