<?php

class TasksController extends \Phalcon\Mvc\Controller
{

    public function indexAction()
    {
        $tasks = Tasks::find();
        $todo = array();
        $process = array();
        $test = array();
        $done = array();
        foreach ($tasks as $task){
            switch ($task->stage){
                case 'process':
                    $process[] = $task;
                    break;
                case 'test':
                    $test[] = $task;
                    break;
                case 'done':
                    $done[] = $task;
                    break;
                default:
                    $todo[] = $task;
                    break;
            }
        }
        $this->view->todo = $todo;
        $this->view->process = $process;
        $this->view->test = $test;
        $this->view->done = $done;
    }// save prject
    public function saveAction() {
        $form = new TaskForm();
        if ($form->isValid($this->request->getPost()) == false) {
            $this->view->disable();
            foreach ($form->getMessages() as $message) {
                $this->flash->error($message);
            }
        } else {
            $task = new Tasks();
            $identity = $this->auth->getIdentity();
            $assigned_to = 0;
            if($this->request->getPost('assigned_to')){
                $assigned_to = $this->request->getPost('assigned_to');
            }
            $task->assign(array(
                'title' => $this->request->getPost('title', 'striptags'),
                'description' => $this->request->getPost('description'),
                'project_id' => $this->request->getPost('project_id'),
                'user_id'=> $identity["id"],
                'assign_to'=> $assigned_to,
                'stage' => 'todo',
                'priority' => 'normal'
                
            ));
            if ($task->save()) {
                $this->view->task = $task;
                $this->view->identity = $identity;
                return;
            }
            $this->flash->error($menu->getMessages());
        }
    }
    // delete Task
    public function deleteAction() {
        if ($this->request->getPost("id")) {
            $id = $this->request->getPost("id");
            if (Tasks::findFirst((int)$id)->delete()) {
                echo "success";
            }
        }
    }
    public function changestageAction(){
        if ($this->request->getPost("id")) {
            $id = $this->request->getPost("id");
            $task = Tasks::findFirst($id);
            $task->stage = $this->request->getPost("stage");
            if($task->save()){
                echo "success";
            }else{
               echo "fail"; 
            }
        }
    }

}

