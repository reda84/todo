<?php

class ProjectsController extends ControllerBase {

    // get project view
    public function indexAction($id) {
        
    }

    public function tasksListAction($id) {
        $this->view->form = new TaskForm();
        $this->view->token = $this->security->getToken();
        $project = Projects::findFirst($id);
        $this->view->project = $project;
        $this->view->tasks = $project->tasks;
    }

    public function listAction() {
        $form = new ProjectForm();
        $this->view->token = $this->security->getToken();
        $this->view->form = $form;
        $this->view->projects = Projects::find();
    }

    // save prject
    public function saveAction() {
        $form = new ProjectForm();
        if ($form->isValid($this->request->getPost()) == false) {
            $this->view->disable();
            foreach ($form->getMessages() as $message) {
                $this->flash->error($message);
            }
        } else {
            $project = new Projects();
            $identity = $this->auth->getIdentity();
            $project->assign(array(
                'project_title' => $this->request->getPost('title', 'striptags'),
                'project_description' => $this->request->getPost('project_description', 'striptags'),
                'user_id' => $identity["id"]
            ));
            if ($project->save()) {
                $this->view->project = $project;
                $this->view->identity = $identity;
                return;
            }
            $this->flash->error($menu->getMessages());
        }
    }

    // delete project
    public function deleteAction() {
        if ($this->request->getPost("id")) {
            $id = $this->request->getPost("id");
            if (Projects::findFirst((int) $id)->delete()) {
                echo "success";
            }
        }
    }

}
