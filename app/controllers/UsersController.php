<?php

class UsersController extends \Phalcon\Mvc\Controller {

    public function indexAction() {
        
    }

    public function meAction() {
        $identity = $this->auth->getIdentity();
        $this->view->user = Users::findFirst($identity["id"]);
        $this->view->identity = $identity;
    }

    public function editAction() {
        $id = $this->request->getPost('id');
        $user = Users::findFirst($id);
        $this->view->form = new ProfileForm($user);
        $this->view->user = $user;
    }

    public function saveAction() {
        $id = $this->request->getPost('id');

        $form = new ProfileForm();
        $user = Users::findFirst($id);
        if ($form->isValid($this->request->getPost()) == false) {
            foreach ($form->getMessages() as $message) {
                $this->flash->error($message);
            }
        } else {
            // Check if the user has uploaded files
            if ($this->request->hasFiles() == true) {

                // Print the real file names and sizes
                foreach ($this->request->getUploadedFiles() as $file) {
                    //Move the file into the application
                    $file->moveTo('public/uploads/users/' . $file->getName());
                }
            }
            $user->username = $this->request->getPost("username");
            $user->email = $this->request->getPost("email");
            $user->avatar_id = $this->request->getPost("avatar_id");
            $user->save();
        }
        $this->view->pick("users/profile");
        $this->view->user = $user;
    }

    function uploadAction() {
        // Check if the user has uploaded files
        if ($this->request->hasFiles() == true) {
            $media = new Media();
            $media->uploadUserAvater($this->request);
        }
    }

}
