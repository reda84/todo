<?php

class IndexController extends \Phalcon\Mvc\Controller {

    public function initialize() {
        //    $this->view->setTemplateBefore('master');
    }

    public function beforeExecuteRoute($dispatcher) {
        $identity = $this->auth->getIdentity();
        if ($identity) {
            return $this->response->redirect('dashboard');
        }
    }

    public function indexAction() {
        
    }

    public function homeAction() {

        $this->view->token = $this->security->getToken();
        $this->view->form = new SignUpForm();
        $this->view->loginForm = new LoginForm();
    }

    public function loginAction() {
        $form = new LoginForm();
        try {
            if (!$this->request->isPost()) {
                if ($this->auth->hasRememberMe()) {
                    return $this->auth->loginWithRememberMe();
                }
            } else {
                if ($form->isValid($this->request->getPost()) == false) {
                    foreach ($form->getMessages() as $message) {
                        $this->flash->error($message);
                    }
                } else {
                    if ($this->auth->check(array(
                                'email' => $this->request->getPost('email'),
                                'password' => $this->request->getPost('password'),
                                'remember' => $this->request->getPost('remember')
                            ))) {
                        return $this->response->redirect('dashboard');
                    }
                }
            }
        } catch (AuthException $e) {
            $this->flash->error($e->getMessage());
        }

        $token = $this->security->getToken();
        $this->view->token = $token;

        $this->view->form = new SignUpForm();
        $this->view->loginForm = new LoginForm();
    }

    public function signupAction() {
        $form = new SignUpForm();
        if ($this->request->isPost()) {
            if ($form->isValid($this->request->getPost()) == false) {
                foreach ($form->getMessages() as $message) {
                    $this->flash->error($message);
                }
            } else {
                $user = new Users();
                $user->assign(array(
                    'username' => $this->request->getPost('username', 'striptags'),
                    'email' => $this->request->getPost('email'),
                    'password' => $this->security->hash($this->request->getPost('password'))
                ));

                if ($user->save()) {
                    return $this->response->redirect('login');
                }
                $this->flash->error($user->getMessages());
            }
        }

        $this->view->token = $this->security->getToken();
        $this->view->form = new SignUpForm();
        $this->view->loginForm = new LoginForm();
    }

    /**
     * Closes the session
     */
    public function logoutAction() {
        $this->auth->remove();
        return $this->response->redirect('');
    }

}
