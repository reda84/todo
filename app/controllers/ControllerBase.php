<?php

use Phalcon\Mvc\Controller;

class ControllerBase extends Controller {

    public function beforeExecuteRoute($dispatcher) {
        $identity = $this->auth->getIdentity();
        if ($identity) {
            if ($this->request->isAjax() == false) {
                $this->view->identity = $identity;
            }
        } else {
            return $this->response->redirect('');
        }
    }

}
