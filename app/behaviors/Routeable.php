<?php

  namespace HHC\Cms\Behaviors;

use Phalcon\Mvc\Model\Behavior,
    Phalcon\Mvc\Model\BehaviorInterface;
use HHC\Models\Cms\Routes;
use HHC\Models\Cms\Resources;
use HHC\Models\Cms\MenusItems;
use HHC\Cms\Aliases;

  class Routeable extends Behavior implements BehaviorInterface {

       /**
        * Receives notifications from the Models Manager
        *
        * @param string $eventType
        * @param Phalcon\Mvc\ModelInterface $model
        */
       public function notify($eventType, $model) {
            if ($eventType == 'afterCreate') {

                 $route = new Routes();
                 $resource = Resources::findFirst($model::RESOURCE_ID);
                 if(!$resource)
                    return;


                  $alias = "";
                  switch ($resource->name) {
                    case 'pages':
                      $alias = (new Aliases())->formatPageAlias($model->getSlug());
                      break;
                    case 'posts':
                      $alias = (new Aliases())->formatPostAlias($model->getSlug());
                      break;
                    
                  }

                 $route->assign(array(
                      'alias' =>   $alias,
                      'namespace' => 'HHC\Controllers\Cms',
                      'controller' => "frontend",
                      'method' => $resource->name,
                      'prams' => json_encode(array($model->id)),
                 ));

                 if ($route->save()) {
                      $model->route_id = $route->id;
                      if ($model->update()) {
                           return $model->id;
                      }
                 }
            }else if( $eventType == "beforeDelete"){
              $route = $model->route;
              if($route)
              {
                
                $route->getMenusItems()->delete();
                $model->route->delete();
              }
                  
              return true;
            }
                 
       }
       public function missingMethod($model, $method, $arguments = array()) {
            // if the method is 'getSlug' convert the title
            if ($method == 'deleteRoute') {
                 $route = Routes::findFirst($arguments[0]);

                 if ($route){
                    MenusItems::find("route_id = '" . $arguments[0] ."'")->delete() ;
                    $route->delete();
                 }
                  
                 return true;
            }
       }

  }
  