<?php

  namespace HHC\Cms\Behaviors;

use Phalcon\Mvc\Model\Behavior,
    Phalcon\Mvc\Model\BehaviorInterface;
use HHC\Models\Cms\Taxonomies;
use HHC\Models\Cms\ObjectsTerms;

  class Taxonomiable extends Behavior implements BehaviorInterface {

       public function notify($eventType, $obj) {
            if ($eventType == 'afterCreate') {
                 
            }
       }

       public function missingMethod($model, $method, $arguments = array()) {
            if ($method == 'getTaxonomyies') {
                 return Taxonomies::find("resource_id = " . $model::RESOURCE_ID);
            } elseif ($method == 'newTaxonomy') {
                 if (!$arguments[0]) {
                      $taxonomy_data = $arguments[0];
                 }
            } elseif ($method == 'saveTerms') {

                 /**
                  * save the object terms
                  */
                 if ($arguments[0]) {
                      $terms = $arguments[0];
                      $i = 0;
                      $model->getTerms()->delete();
                      foreach ($terms as $term) {
                           $ot[$i] = new ObjectsTerms();
                           $ot[$i]->assign(array(
                                "resource_id" => $model::RESOURCE_ID,
                                "term_id" => $term
                           ));
                           $i++;
                      }
                      $model->terms = $ot;
                      $model->save();
                 }
                 return true;
            } elseif ($method == 'getTermsIds') {
                 $terms = array();
                 foreach ($model->terms as $object_term) {
                      $term = $object_term->term;
                      if (!array_key_exists($term->taxonomy_id, $terms))
                           $terms[$term->taxonomy_id] = array();
                      $terms[$term->taxonomy_id][] = $term->id;
                 }
                 return $terms;
            }elseif ($method == 'TermsHtmlListByTaxonomy') {
                 $html = "";
                 if ($arguments[0]) {
                      $tax_id = $arguments[0];

                      $taxonomy = Taxonomies::findFirst($tax_id);
                      if ($taxonomy) {
                           $terms = $taxonomy->getTerms();
                           $current_terms_ids = $model->getTermsIds();
                           if(array_key_exists($tax_id,$current_terms_ids))
                              $current_terms_ids = $current_terms_ids[$tax_id];
                           else 
                              $current_terms_ids = array();
                           
                           
                           foreach ($terms as $term) {
                                $checked = "";
                                if (in_array($term->id, $current_terms_ids))
                                     $checked = 'checked="checked"';
                                $html .= '<div class="term">';
                                $html .= '<input name="terms[]" type="checkbox" value="' . $term->id . '" ' . $checked . ' />';
                                $html .= $term->name . "</div>";
                           }
                      }
                 }
                 return $html;
            }
       }

  }
  