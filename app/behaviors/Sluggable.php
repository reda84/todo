<?php

use Phalcon\Mvc\Model\Behavior,
    Phalcon\Mvc\Model\BehaviorInterface;

class Sluggable extends Behavior implements BehaviorInterface {

    protected $slug_field;
    protected $title_field;

    function __construct($options = null) {
        parent::__construct($options);
        if (count($options)) {
            $this->slug_field = ($options["slug"]) ? $options["slug"] : "slug";
            $this->title_field = ($options["title"]) ? $options["title"] : "title";
        }
    }

    public function missingMethod($model, $method, $arguments = array()) {
        // if the method is 'getSlug' convert the title
        if ($method == 'generateSlug') {
            $title_field = $this->title_field;
            $slug_field = $this->slug_field;

            $slug = \Phalcon\Tag::friendlyTitle(trim($model->$title_field));

            $find = $model->findFirst($slug_field . "='" . $slug . "'");
            $num = 2;
            $orginal_slug = $slug;
            while ($find) {
                $slug = $orginal_slug . "-" . $num;
                $find = $model->findFirst($slug_field . "='" . $slug . "'");
                $num++;
            }
            return $slug;
        } elseif ($method == 'setSlugFeild') {
            if ($arguments[0]) {
                $this->slug_field = $arguments[0];
            }
            return true;
        } elseif ($method == 'setTitleFeild') {
            if ($arguments[0]) {
                $this->title_field = $arguments[0];
            }
        } elseif ($method == 'getSlug') {
            $slug_field = $this->slug_field;
            return $model->$slug_field;
        }
    }

}
