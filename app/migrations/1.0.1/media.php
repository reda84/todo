<?php 

use Phalcon\Db\Column;
use Phalcon\Db\Index;
use Phalcon\Db\Reference;
use Phalcon\Mvc\Model\Migration;

class MediaMigration_101 extends Migration
{

    public function up()
    {
        $this->morphTable(
            'media',
            array(
            'columns' => array(
                new Column(
                    'id',
                    array(
                        'type' => Column::TYPE_INTEGER,
                        'notNull' => true,
                        'autoIncrement' => true,
                        'size' => 10,
                        'first' => true
                    )
                ),
                new Column(
                    'module_name',
                    array(
                        'type' => Column::TYPE_VARCHAR,
                        'size' => 1000,
                        'after' => 'id'
                    )
                ),
                new Column(
                    'filename',
                    array(
                        'type' => Column::TYPE_VARCHAR,
                        'size' => 1000,
                        'after' => 'module_name'
                    )
                ),
                new Column(
                    'task_id',
                    array(
                        'type' => Column::TYPE_INTEGER,
                        'size' => 10,
                        'after' => 'filename'
                    )
                ),
                new Column(
                    'created_at',
                    array(
                        'type' => Column::TYPE_VARCHAR,
                        'size' => 1,
                        'after' => 'task_id'
                    )
                )
            ),
            'indexes' => array(
                new Index('PRIMARY', array('id')),
                new Index('id', array('id')),
                new Index('filename', array('filename'))
            ),
            'options' => array(
                'TABLE_TYPE' => 'BASE TABLE',
                'AUTO_INCREMENT' => '96',
                'ENGINE' => 'InnoDB',
                'TABLE_COLLATION' => 'latin1_swedish_ci'
            )
        )
        );
    }
}
