<?php 

use Phalcon\Db\Column;
use Phalcon\Db\Index;
use Phalcon\Db\Reference;
use Phalcon\Mvc\Model\Migration;

class CiSessionsMigration_101 extends Migration
{

    public function up()
    {
        $this->morphTable(
            'ci_sessions',
            array(
            'columns' => array(
                new Column(
                    'session_id',
                    array(
                        'type' => Column::TYPE_VARCHAR,
                        'notNull' => true,
                        'size' => 40,
                        'first' => true
                    )
                ),
                new Column(
                    'ip_address',
                    array(
                        'type' => Column::TYPE_VARCHAR,
                        'notNull' => true,
                        'size' => 16,
                        'after' => 'session_id'
                    )
                ),
                new Column(
                    'user_agent',
                    array(
                        'type' => Column::TYPE_VARCHAR,
                        'notNull' => true,
                        'size' => 250,
                        'after' => 'ip_address'
                    )
                ),
                new Column(
                    'last_activity',
                    array(
                        'type' => Column::TYPE_INTEGER,
                        'unsigned' => true,
                        'notNull' => true,
                        'size' => 10,
                        'after' => 'user_agent'
                    )
                ),
                new Column(
                    'user_data',
                    array(
                        'type' => Column::TYPE_TEXT,
                        'notNull' => true,
                        'size' => 1,
                        'after' => 'last_activity'
                    )
                )
            ),
            'indexes' => array(
                new Index('PRIMARY', array('session_id'))
            ),
            'options' => array(
                'TABLE_TYPE' => 'BASE TABLE',
                'AUTO_INCREMENT' => '',
                'ENGINE' => 'InnoDB',
                'TABLE_COLLATION' => 'latin1_swedish_ci'
            )
        )
        );
    }
}
