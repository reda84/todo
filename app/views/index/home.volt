{% extends "layouts/master.volt" %}
{% block content %}
<div class="landing-container">
    <section class="container-fluid">
        <div class="row-fluid">
            <section class="col-md-12">

                {{ content() }}
                <h2>Welcom to Scurm TODO</h2>

                <!-- Login block -->
                <div class="login-form">
                    <div class="well">
                        <h3>Login</h3>
                        <form method="post" action="/login" >

                            <div class="control-group">
                                <label class="control-label">Email</label>
                                <div class="controls">
                                    {{ loginForm.render('email') }}

                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">Password:</label>
                                <div class="controls">
                                    {{ loginForm.render('password') }}

                                </div>
                            </div>


                            <div class="control-group">
                                <div class="controls">  
                                    <label class="checkbox inline">

                                        {{ loginForm.render('remember') }}

                                        Remember me</label>     


                                    <label class="pull-right inline checkbox">
                                        <div class="forgot">
                                            {{ link_to("session/forgotPassword", "Forgot my password") }}
                                        </div>
                                    </label>
                                </div>


                            </div>

                            <div class="login-btn">
                                {{ loginForm.render('Login') }}
                                {{ loginForm.render('csrf', ['value': token]) }}
                            </div>
                        </form>

                    </div>
                </div>

                <!-- Login block -->
                <div class="login-form">

                    <div class="well">
                        <h3>Create New Account</h3>
                        <form method="post" action="/signup" >
                            <div class="control-group">
                                <label class="control-label">Username</label>
                                <div class="controls">
                                    {{ form.render('username') }}
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Email</label>
                                <div class="controls">
                                    {{ form.render('email') }}

                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">Password:</label>
                                <div class="controls">
                                    {{ form.render('password') }}

                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Password:</label>
                                <div class="controls">
                                    {{ form.render('confirmPassword') }}

                                </div>
                            </div>

                            <div class="control-group">
                                <div class="controls">  
                                    <label class="checkbox inline">

                                        {{ form.render('terms') }}  

                                        Accept terms and conditions</label>     
                                </div>
                            </div>
                            <div class="login-btn">
                                {{ form.render('Sign Up') }}
                                {{ form.render('csrf', ['value': token]) }}
                            </div>
                        </form>

                    </div>
                </div>
            </section>
        </div>
    </section>
</div>
{% endblock %}