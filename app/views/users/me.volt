<br />
<br />
<h3>About Me </h3>
<div id="edit-me-wrap">
    {{ partial("users/profile") }}
</div>
<input type="hidden" name="user_id" id="user-id" value="{{ user.id }}" />


<a href="#edit-me-wrap" id="edit-profile" data-url="/users/edit" class="btn btn-primary">Edit</a>    

<script type="text/javascript" src="/assets/js/app/ajax.js"></script>
<script type="text/javascript" src="/assets/js/ajaxupload.js"></script>

<script type="text/javascript" src="/assets/js/plugins/uploader/plupload.js"></script>
<script type="text/javascript" src="/assets/js/plugins/uploader/plupload.html4.js"></script>
<script type="text/javascript" src="/assets/js/plugins/uploader/plupload.html5.js"></script>
<script type="text/javascript" src="/assets/js/plugins/uploader/jquery.plupload.queue.js"></script>

<script type="text/javascript" src="/assets/js/app/users.js"></script>






