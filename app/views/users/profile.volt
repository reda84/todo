{{ content() }}
{% if user.avatar %}
    <img src="/uploads/users/{{ user.avatar.name }}" class="thumbnail" width="150" />
    {% else %}
    <img src="/uploads/users/default.jpg" class="thumbnail" width="150" />
{% endif %}
<label>Username</label>: {{ user.username }} <br />
<label>Email</label>: {{ user.email }} <br />
<label>Role</label>: {{ user.profile.name }} <br />    