<form action="/users/save" method="post" id="save-profile" enctype="multipart/form-data">
    {{ content() }}
    <div class="row-fluid">
        <div class="col-md-6">
            <div class="control-group">
                <label class="control-label">Upload thumbnail</label>
                <div class="controls">
                    {% if user.avatar %}
                        <img src="/uploads/users/{{ user.avatar.name }}" class="thumbnail" id="user-avatar" width="150" />
                        <input type="hidden" name="avatar_id" id="avatar_id" value="{{ user.avatar.id }}" />
                        
                    {% else %}
                        <img src="/uploads/users/default.jpg" class="thumbnail" id="user-avatar" width="150" />
                        <input type="hidden" name="avatar_id" id="avatar_id" value="0" />
                    {% endif %}
                    <a href="/users/upload" id="upload_avatar" class="btn btn-primary inline upload-button">Upload</a>
                </div>
            </div>

            <div class="control-group">
                <label class="control-label">UserName</label>
                <div class="controls">
                    {{ form.render('username') }}
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Email</label>
                <div class="controls">
                    {{ form.render('email') }}
                </div>
            </div>
            <br />
            <div class="login-btn">
                {{ form.render('Update') }}
                <input type="hidden" value="{{ user.id }}" name="id" />
            </div>        
        </div>
    </div>
</form>
<style>
    .file-uploader_filelist{display: none !important;}
    .plupload_filelist_header{display: none !important;}
</style>
<script>
    jQuery(function($) {
        $('.upload-button').each(function() {
            hhcUsers.uploadAvatar($(this));
        });

    })

</script>

