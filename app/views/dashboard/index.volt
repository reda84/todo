{% extends "layouts/master.volt" %}

{% block content %}

<!-- PAGELOADER -->
<div id="page-loader" class="content-loader">
    <div class="page-loader-inner">
        <div class="loader-logo">

        </div>
        <div class="loader-icon">
            <img src="/assets/img/loading.gif" />
            </span></div>
    </div>
</div>
<!-- PAGELOADER -->

<div id="page-content"> 
    <ul class="nav nav-tabs" id="myTab">
        <li><a href="#dashboard"  data-toggle="tab" data-url="/dashboard/summary">Dashboard</a></li>
        <li><a href="#sprints" class="disabled" data-toggle="tab" data-url="/sprints">Sprints</a></li>
        <li><a href="#backlog" class="disabled" data-toggle="tab" data-url="/backlog">backlog</a></li>
        <li class="active"><a href="#projects" data-toggle="tab" data-url="/projects/list">Projects</a></li>
        <li><a href="#profile" class="disabled" data-toggle="tab" data-url="/people">People</a></li>
        <li><a href="#tasksboard" data-toggle="tab" data-url="/tasks">Tasks Board</a></li>
        <li><a href="#burndown" class="disabled" data-toggle="tab" data-url="/burndown">Burn Down</a></li>
        <li><a href="#me"  data-toggle="tab" data-url="/users/me">Me</a></li>

    </ul>
    <div class="tab-content">
        <div class="tab-pane fade" id="dashboard"></div>
        <div class="tab-pane fade in active" id="projects"></div>
        <div class="tab-pane fade" id="tasksboard"></div>
        <div class="tab-pane fade" id="me"></div>
    </div>


   <script type="text/javascript" src="/assets/js/app/ajax.js"></script>
    <script>
        $(function() {
            // adjust screen layout
            $("#projects").innerHeight($("#page-content").height() - $("#myTab").height())
            $(window).resize(function() {
                $("#projects").innerHeight($("#page-content").height() - $("#myTab").height())
                       $("#projects").mCustomScrollbar("update"); 
            });

           

            $(document).on("click", ".project-head a", function(){
                showDashboardTab($(this));
                return false;
            });
            var firstTab = $('#myTab a.active');
            firstTab.tab('show');
            showDashboardTab(firstTab);
           
            $('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
                if($(e.target).hasClass("disabled"))
                    return false;
                showDashboardTab($(e.target));
                return false;
            });
            function showDashboardTab(a){
                hhcAjax.showTab(a,function(data) {
                    var target = $(a).attr("href")
                    $(target).html(data);
                    $("#projects").mCustomScrollbar({
                        "mouseWheelPixels": 300
                    });
                });
            }
           
        })
    </script>
</div>
{% endblock %}
{% block footer %}&copy; Copyright 2014, hello{% endblock %}
