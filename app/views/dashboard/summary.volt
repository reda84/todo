
<br />
<br />


<div class="row-fluid">
    <div class="col-md-6">
        <div class="widget">
            <div class="navbar"><div class="navbar-inner"><h6>Burndown</h6></div></div>
            
                <div id="chartdiv"></div>    
            
        </div>    
    </div>
    <div class="col-md-6">
        <div class="widget">
            <div class="navbar"><div class="navbar-inner"><h6>Last Activities</h6></div></div>
            <div class="activity"><a href="#">Reda</a> Start Task <a href="#">Task1</a></div>
            <div class="activity"><a href="">Aaron</a> Complete Task <a href="#">Task2</a></div>
            <div class="activity">new task Assigned to <a href="#">Reda</a></div>
        </div>    
    </div>
</div>    





<link rel="stylesheet" href="/assets/js/style.css" type="text/css">
<script src="/assets/js/amcharts/amcharts.js" type="text/javascript"></script>
<script src="/assets/js/amcharts/serial.js" type="text/javascript"></script>

<script type="text/javascript">
    var chart;

    var chartData = [];

//            AmCharts.ready(function () {
    // generate some random data first
    generateChartData();

    // SERIAL CHART
    chart = new AmCharts.AmSerialChart();
    chart.pathToImages = "../amcharts/images/";
    chart.marginLeft = 0;
    chart.marginRight = 0;
    chart.marginTop = 0;
    chart.dataProvider = chartData;
    chart.categoryField = "date";

    // AXES
    // category
    var categoryAxis = chart.categoryAxis;
    categoryAxis.parseDates = true; // as our data is date-based, we set parseDates to true
    categoryAxis.minPeriod = "DD"; // our data is daily, so we set minPeriod to DD
    // value axis
    var valueAxis = new AmCharts.ValueAxis();
    valueAxis.inside = false;
    valueAxis.tickLength = 1;
    valueAxis.axisAlpha = .5;
    valueAxis.autoGridCount = true;
    chart.addValueAxis(valueAxis);

//                // Distance value axis
//                var distanceAxis = new AmCharts.ValueAxis();
//                distanceAxis.title = "distance";
//                distanceAxis.gridAlpha = 0;
//                distanceAxis.axisAlpha = 0;
//                chart.addValueAxis(distanceAxis);

    // GRAPH
    var graph = new AmCharts.AmGraph();
    graph.dashLength = 3;
    graph.lineColor = "#7717D7";
    graph.valueField = "visits";
    graph.dashLength = 3;
    graph.bullet = "round";
    chart.addGraph(graph);

    // distance graph
    var distanceGraph = new AmCharts.AmGraph();
    distanceGraph.valueField = "distance";
    distanceGraph.title = "distance";
    distanceGraph.type = "column";
    distanceGraph.fillAlphas = 0.9;
    distanceGraph.valueAxis = valueAxis; // indicate which axis should be used
    distanceGraph.balloonText = "[[value]] days";
    distanceGraph.legendValueText = "[[value]] mi";
    distanceGraph.legendPeriodValueText = "total: [[value.sum]] mi";
    distanceGraph.lineColor = "#B4D656";
    distanceGraph.dashLengthField = "dashLength";
    distanceGraph.alphaField = "alpha";
    chart.addGraph(distanceGraph);

    // CURSOR
    var chartCursor = new AmCharts.ChartCursor();
    chartCursor.cursorAlpha = 0;
    chart.addChartCursor(chartCursor);

    // WRITE
    chart.write("chartdiv");
//            });

    // generate some random data
    function generateChartData() {
        var firstDate = new Date();
        firstDate.setDate(firstDate.getDate() - 10);

        for (var i = 0, j = 20; i < 20; i++, j--) {



            var newDate = new Date(firstDate);
            newDate.setDate(newDate.getDate() + i);

            var visits = j;

            var tolerance = Math.floor(Math.random() * (1 - 0 + 1)) + 0;
            ;
            tolerance *= Math.floor(Math.random() * 2) == 1 ? 1 : -1;
            chartData.push({
                date: newDate,
                visits: visits,
                distance: visits + tolerance
            });
        }
    }
</script>
