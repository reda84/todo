<div class="item-box" id="task-<?php echo $task->id; ?>">
    <h3 class="item-head">
        <?php echo $task->title; ?>
        <span class="actions pull-right">
            <a href="#" class="delete-project" data-id="<?php echo $task->id; ?>"><i class="fam-delete"></i></a>
        </span>
    </h3>
    <div class="item-desc">
        <?php echo $task->description; ?>
    </div>
</div>