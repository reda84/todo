{%- macro user_avatar(user, width) %}
    {% if user.avatar %}
        <img src="/uploads/users/{{ user.avatar.name }}" class="thumbnail" id="user-avatar" width="{{width}}" />
        <input type="hidden" name="avatar_id" id="avatar_id" value="{{ user.avatar.id }}" />
    {% else %}
        <img src="/uploads/users/default.jpg" class="thumbnail" id="user-avatar" width="{{width}}" />
        <input type="hidden" name="avatar_id" id="avatar_id" value="0" />
    {% endif %}
{%- endmacro %}

{%- macro render_task(task) %}
    <div class="pipeline-item item-box clearfix" data-id="{{task.id}}">
        <div class="pull-right">
            {% if task.user %}
                {{ user_avatar(task.user, 60) }}
            {% endif %}    
        </div>
        <a data-toggle="modal" href="#edit-details" >{{task.title}}</a><br />
    </div>
{%- endmacro %}


<div id="pipline" class="clearfix">
    <div class="pipline-column">
        <h3 class="pipline-column-heading">
            ToDo
            <span class="pipline-heading-arrow"></span>
        </h3>
        <div class="pipeline-column-content sortable-column">
            {% for task in todo %}
                {{ render_task(task) }}
            {% endfor %}
            <input type="hidden" name="stage" class="stage" value="todo" />
        </div>
        
    </div>
    <div class="pipline-column">
        <h3 class="pipline-column-heading">
            In Progress
            <span class="pipline-heading-arrow"></span>
        </h3>
        <div class="pipeline-column-content sortable-column">
            {% for task in process %}
                {{ render_task(task) }}
            {% endfor %}
            <input type="hidden" name="stage" class="stage" value="process" />
        </div>
        
    </div>
    <div class="pipline-column">
        <h3 class="pipline-column-heading">
            To Test
            <span class="pipline-heading-arrow"></span>
        </h3>
        <div class="pipeline-column-content sortable-column">
            {% for task in test %}
                {{ render_task(task) }}
            {% endfor %}
                    <input type="hidden" name="stage" class="stage" value="test" />
        </div>

    </div>
    <div class="pipline-column">
        <h3 class="pipline-column-heading">
            Done
            <span class="pipline-heading-arrow"></span>
        </h3>
        <div class="pipeline-column-content sortable-column">
            {% for task in done %}
                {{ render_task(task) }}
            {% endfor %}
        <input type="hidden" name="stage" class="stage" value="done" />    
        </div>
        
    </div>
</div>
<script type="text/javascript" src="/assets/js/app/ajax.js"></script>
<script type="text/javascript" src="/assets/js/app/pipeline.js"></script>