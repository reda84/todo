<link rel="stylesheet" type="text/css" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css" />
<link rel="stylesheet" type="text/css" href="/assets/css/custom.css" />
<link rel="stylesheet" type="text/css" href="/assets/css/fam-icons.css" />

<script type="text/javascript" src="//code.jquery.com/jquery-1.10.2.js"></script>
<script type="text/javascript" src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script type="text/javascript" src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery.easytabs/3.2.0/jquery.easytabs.min.js"></script>
<script type="text/javascript" src="/assets/ckeditor/ckeditor.js"></script>

<link rel="stylesheet" type="text/css" href="/assets/js/scroll/jquery.mCustomScrollbar.css" />
<script type="text/javascript" src="/assets/js/scroll/jquery.mCustomScrollbar.concat.min.js"></script>
<!--<script type="text/javascript" src="/assets/js/nicescroll/jquery.nicescroll.min.js"></script>-->


<script type="text/javascript" src="/assets/js/script.js"></script>