<?php if (isset($project)) :?>
    <div class="project" id="project-<?php echo $project->id; ?>">
        <h3 class="project-head">
            <a href="#projects" data-url="/projects/taskslist/<?php echo $project->id; ?>"><?php echo $project->project_title; ?></a>
            <span class="actions pull-right">
                <a href="#" class="delete-project" data-id="<?php echo $project->id; ?>"><i class="fam-delete"></i></a>
            </span>
        </h3>
        <div class="project-desc">
            <?php echo $project->project_description; ?>
        </div>
    </div>
<?php endif; ?>
