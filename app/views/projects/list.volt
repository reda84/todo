<div class="container-fuild">
    <div class="row-fluid">
        <div class="col-md-8">
            <h1>Projects list</h1>
            <div id="projects-list">

                <?php foreach ($projects as $project): ?>
                    <div class="project" id="project-<?php echo $project->id; ?>">
                        <h3 class="project-head">
                            <a href="#projects" data-url="/projects/taskslist/<?php echo $project->id; ?>"><?php echo $project->project_title; ?></a>
                            <span class="actions pull-right">
                                    <a href="#" class="delete-project" data-id="<?php echo $project->id; ?>"><i class="fam-delete"></i></a>
                            </span>
                        </h3>
                        <div class="project-desc">
                            <?php echo $project->project_description; ?>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
        <div class="col-md-4">
            <form action="/projects/save" id="add-project-form" method="post">
                {{ form.render("title")  }}
                <br />
                {{ form.render("project_description")  }}
                <br />
                <input id="csrf" type="hidden" name="csrf" value="{{ token }}">

                {{ form.render("Add Project")  }}
            </form>
        </div>
    </div>
</div>
<script type="text/javascript" src="/assets/js/app/ajax.js"></script>
<script type="text/javascript" src="/assets/js/app/projects.js"></script>

<script type="text/javascript">
   $(function()
   {
        CKEDITOR.replace('project_description');
   });
</script>