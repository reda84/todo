<div class="container-fuild">
    <div class="row-fluid">
        <div class="col-md-8">
            <h2 class="pull-right inline">
                <a data-toggle="tab" href="#projects" data-url="/projects/list" >Back to Projects</a>
            </h2>
            <h2 class="inline">Tasks list</h2>
            <div id="tasks-list">
                <?php foreach ($tasks as $task): ?>
                    <div class="item-box" id="task-<?php echo $task->id; ?>">
                        <h3 class="item-head">
                            <a href="/prjects/<?php echo $task->id; ?>"><?php echo $task->title; ?></a>
                            <span class="actions pull-right">
                                <a href="#" class="delete-task" data-id="<?php echo $task->id; ?>"><i class="fam-delete"></i></a>
                            </span>
                        </h3>
                        <div class="item-desc">
                            <?php echo $task->description; ?>
                        </div>
                        <input type="hidden" class="task-order" id="task-order-{{ task.order }}" value="{{ task.order }}" />
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
        <div class="col-md-4">
            <form action="/tasks/save" id="add-task-form" method="post">
                {{ form.render("title")  }}
                <br />
                {{ form.render("description")  }}
                <br />
                {{ form.render("assigned_to")  }}
                <br />
                <input id="csrf" type="hidden" name="csrf" value="{{ token }}">


                <input type="hidden" value="{{ project.id}}" name="project_id" />
                {{ form.render("Add Project")  }}
            </form>
        </div>
    </div>
</div>
<script type="text/javascript" src="/assets/js/app/ajax.js"></script>
<script type="text/javascript" src="/assets/js/app/tasks.js"></script>
<script>
jQuery(function($) {
    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
        if ($(e.target).hasClass("disabled"))
            return false;

        hhcAjax.showTab($(e.target), function(data) {
            var target = $($(e.target)).attr("href")
            $(target).html(data);
            $("#projects").mCustomScrollbar({
                "mouseWheelPixels": 300
            });
        });
        return false;
    });
});

</script>