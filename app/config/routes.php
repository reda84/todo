<?php 
$router = new Phalcon\Mvc\Router(true);

$router->add('/login', array( 'controller' => "index", 'action' => "login" ));
$router->add('/', array( 'controller' => "index", 'action' => "home" ));
$router->add('/logout', array( 'controller' => "index", 'action' => "logout" ));
$router->add('/signup', array( 'controller' => "index", 'action' => "signup" ));


return $router;
