<?php

$env = ( $_SERVER["SERVER_ADDR"] == "127.0.0.1" ) ? "dev" : "prod";

 $config = new \Phalcon\Config(array(
    'application' => array(
        'controllersDir' => __DIR__ . '/../../app/controllers/',
        'modelsDir'      => __DIR__ . '/../../app/models/',
        'viewsDir'       => __DIR__ . '/../../app/views/',
        'pluginsDir'     => __DIR__ . '/../../app/plugins/',
        'libraryDir'     => __DIR__ . '/../../app/library/',
        'behaviorsDir'     => __DIR__ . '/../../app/behaviors/',
        'formsDir'     => __DIR__ . '/../../app/forms/',
        'cacheDir'       => __DIR__ . '/../../app/cache/',
        'baseUri'        => '/',
    )
));

switch ($env) {
    case 'dev':
    $config["database"] = array(
                            'adapter'     => 'Mysql',
                            'host'        => 'localhost',
                            'username'    => 'root',
                            'password'    => '',
                            'dbname'      => 'todos',
                        );
    break;
    case 'prod':
    $config["database"] = array(
                            'adapter'     => 'Mysql',
                            'host'        => 'hhc-phalcon.mysql.eu1.frbit.com',
                            'username'    => 'hhc-phalcon',
                            'password'    => 'fc1gHeWFnOR9V1IO',
                            'dbname'      => 'hhc-phalcon',
                        );
    break;
    default:
    $config["database"] = array(
                            'adapter'     => 'Mysql',
                            'host'        => 'localhost',
                            'username'    => 'root',
                            'password'    => '',
                            'dbname'      => 'todos',
                        );
    break;
}

return $config;
