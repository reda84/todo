<?php

use Phalcon\Mvc\User\Component;

/**
 * HHC\Users\Auth\Auth
 * Manages Authentication/Identity Management in HHC\Users
 */
class Auth extends Component {

    /**
     * Checks the user credentials
     *
     * @param array $credentials
     * @return boolan
     */
    public function check($credentials) {


        // Check if the user exist
        $user = Users::findFirstByEmail($credentials['email']);

        if ($user == false) {
            //  $this->registerUserThrottling(0);
            $this->flash->error(array('User does not Exist'));
            return false;
        }

        // Check the password
        if (!$this->security->checkHash($credentials['password'], $user->password)) {
            // $this->registerUserThrottling($user->id);
            $this->flash->error(array('Wrong email/password combination'));
            return false;
        }

        // Check if the user was flagged
        $this->checkUserFlags($user);


        // Register the successful login
        // $this->saveSuccessLogin($user);
        // Check if the remember me was selected
        if (isset($credentials['remember'])) {
            $this->createRememberEnviroment($user);
        }

        // Register identity
        $this->session->set('auth-identity', array(
            'id' => $user->id,
            'name' => $user->username,
            'profile' => $user->profile->name
        ));
        return true;
    }

    /**
     * Creates the remember me environment settings the related cookies and generating tokens
     *
     * @param HHC\Models\Users\Users $user
     */
    public function saveSuccessLogin($user) {

        $successLogin = new SuccessLogins();
        $successLogin->users_id = $user->id;

        $successLogin->ip_address = $this->request->getClientAddress();
        $successLogin->user_agent = $this->request->getUserAgent();
        if (!$successLogin->create()) {

            $messages = $successLogin->getMessages();
            throw new Exception($messages[0]);
        }
    }

    /**
     * Implements login throttling
     * Reduces the efectiveness of brute force attacks
     *
     * @param int $userId
     */
    public function registerUserThrottling($userId) {
        $failedLogin = new FailedLogins();
        $failedLogin->users_id = $userId;
        $failedLogin->ip_address = $this->request->getClientAddress();
        $failedLogin->attempted = time();
        $failedLogin->save();

        $attempts = FailedLogins::count(array(
                    'ip_address = ?0 AND attempted >= ?1',
                    'bind' => array(
                        $this->request->getClientAddress(),
                        time() - 3600 * 6
                    )
        ));

        switch ($attempts) {
            case 1:
            case 2:
                // no delay
                break;
            case 3:
            case 4:
                sleep(2);
                break;
            default:
                sleep(4);
                break;
        }
    }

    /**
     * Creates the remember me environment settings the related cookies and generating tokens
     *
     * @param HHC\Models\Users\Users $user
     */
    public function createRememberEnviroment(Users $user) {
        $user_agent = $this->request->getUserAgent();
        $token = md5($user->email . $user->password . $user_agent);

        $remember = new RememberTokens();
        $remember->user_id = $user->id;
        $remember->token = $token;
        $remember->user_agent = $user_agent;

        if ($remember->save() != false) {
            $expire = time() + 86400 * 8;
            $this->cookies->set('RMU', $user->id, $expire);
            $this->cookies->set('RMT', $token, $expire);
        }
    }

    /**
     * Check if the session has a remember me cookie
     *
     * @return boolean
     */
    public function hasRememberMe() {
        return $this->cookies->has('RMU');
    }

    /**
     * Logs on using the information in the coookies
     *
     * @return Phalcon\Http\Response
     */
    public function loginWithRememberMe() {
        $userId = $this->cookies->get('RMU')->getValue();
        $cookieToken = $this->cookies->get('RMT')->getValue();

        $user = Users::findFirstById($userId);
        if ($user) {

            $user_agent = $this->request->getuser_agent();
            $token = md5($user->email . $user->password . $user_agent);

            if ($cookieToken == $token) {

                $remember = RememberTokens::findFirst(array(
                            'user_id = ?0 AND token = ?1',
                            'bind' => array(
                                $user->id,
                                $token
                            )
                ));
                if ($remember) {

                    // Check if the cookie has not expired
                    if ((time() - (86400 * 8)) < $remember->createdAt) {

                        // Check if the user was flagged
                        $this->checkUserFlags($user);

                        // Register identity
                        $this->session->set('auth-identity', array(
                            'id' => $user->id,
                            'name' => $user->name,
                            'profile' => $user->profile->name
                        ));

                        // Register the successful login
                        //$this->saveSuccessLogin($user);

                        return $this->response->redirect('users');
                    }
                }
            }
        }

        $this->cookies->get('RMU')->delete();
        $this->cookies->get('RMT')->delete();

        return $this->response->redirect('login');
    }

    /**
     * Checks if the user is banned/inactive/suspended
     *
     * @param HHC\Models\Users\Users $user
     */
    public function checkUserFlags(Users $user) {
        return true;
        if ($user->active != 'Y') {
            throw new Exception('The user is inactive');
        }

        if ($user->banned != 'N') {
            throw new Exception('The user is banned');
        }

        if ($user->suspended != 'N') {
            throw new Exception('The user is suspended');
        }
    }

    /**
     * Returns the current identity
     *
     * @return array
     */
    public function getIdentity() {
        return $this->session->get('auth-identity');
    }

    /**
     * Returns the current identity
     *
     * @return string
     */
    public function getName() {
        $identity = $this->session->get('auth-identity');
        return $identity['name'];
    }

    /**
     * Removes the user identity information from session
     */
    public function remove() {
        if ($this->cookies->has('RMU')) {
            $this->cookies->get('RMU')->delete();
        }
        if ($this->cookies->has('RMT')) {
            $this->cookies->get('RMT')->delete();
        }
        $this->session->remove('auth-identity');
    }

    /**
     * Auths the user by his/her id
     *
     * @param int $id
     */
    public function authUserById($id) {
        $user = Users::findFirstById($id);
        if ($user == false) {
            throw new Exception('The user does not exist');
        }

        $this->checkUserFlags($user);

        // Register identity
        $this->session->set('auth-identity', array(
            'id' => $user->id,
            'name' => $user->name,
            'profile' => $user->profile->name
        ));
    }

    /**
     * Get the entity related to user in the active identity
     *
     * @return \HHC\Models\Users\Users
     */
    public function getUser() {
        $identity = $this->session->get('auth-identity');
        if (isset($identity['id'])) {

            $user = Users::findFirstById($identity['id']);
            if ($user == false) {
                throw new Exception('The user does not exist');
            }
            return $user;
        }
        return false;
    }

}
