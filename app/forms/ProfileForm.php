<?php

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Hidden;
use Phalcon\Forms\Element\Password;
use Phalcon\Forms\Element\Submit;
use Phalcon\Forms\Element\Check;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Email;
use Phalcon\Validation\Validator\Identical;
use Phalcon\Validation\Validator\StringLength;
use Phalcon\Validation\Validator\Confirmation;

class ProfileForm extends Form {

    public function initialize($entity = null, $options = null) {
        $username = new Text('username', array("class" => "form-control"));
        $username->addValidators(array(
            new PresenceOf(array(
                'message' => 'The username is required'
                    ))
        ));
        $this->add($username);
        $email = new Text('email', array(
            "class" => "form-control"
        ));
        $email->addValidators(array(
            new PresenceOf(array(
                'message' => 'The e-mail is required'
                    )),
            new Email(array(
                'message' => 'The e-mail is not valid'
                    ))
        ));
        $this->add($email);
      

        // Sign Up
        $this->add(new Submit('Update', array(
            'class' => 'btn btn-success'
        )));
    }

    /**
     * Prints messages for a specific element
     */
    public function messages($name) {
        if ($this->hasMessagesFor($name)) {
            foreach ($this->getMessagesFor($name) as $message) {
                $this->flash->error($message);
            }
        }
    }

}
