<?php
use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Password;
use Phalcon\Forms\Element\Submit;
use Phalcon\Forms\Element\Check;
use Phalcon\Forms\Element\Select;
use Phalcon\Forms\Element\TextArea;
use Phalcon\Forms\Element\Hidden;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Email;
use Phalcon\Validation\Validator\Identical;

class TaskForm extends Form
{
	public function initialize()
    {
        // Email
        $title = new Text('title', array(
            'placeholder' => 'Task Title',
            'class' => 'form-control'
        ));

        $title->addValidators(array(
            new PresenceOf(array(
                'Title' => 'The Title is required'
            )),
        ));
        $assigned_to = new Select("assigned_to", Users::find(), array(
               'useEmpty'  => true,
               'emptyText' => 'Dont assign',
               'class' => 'form-control',
               'using' => array("id", "username")
            ));        
        $editor = new TextArea("description",array("class"=>"ckeditor"));

        // CSRF
        $csrf = new Hidden('csrf');
        $csrf->addValidator(new Identical(array(
             'value' => $this->security->getSessionToken(),
             'message' => 'CSRF validation failed'
        )));
        
        $this->add($title);
        $this->add($assigned_to);
        $this->add($editor);
        $this->add($csrf);
        
        // Sign Up
        $this->add(new Submit('Add Project', array(
             'class' => 'btn btn-success'
        )));
    }
}