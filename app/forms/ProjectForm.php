<?php
use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Password;
use Phalcon\Forms\Element\Submit;
use Phalcon\Forms\Element\Check;
use Phalcon\Forms\Element\TextArea;
use Phalcon\Forms\Element\Hidden;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Email;
use Phalcon\Validation\Validator\Identical;

class ProjectForm extends Form
{
	public function initialize()
    {
        // Email
        $title = new Text('title', array(
            'placeholder' => 'Project Title',
             'class' => 'form-control'
        ));

        $title->addValidators(array(
            new PresenceOf(array(
                'Title' => 'The Title is required'
            )),
        ));
        
        $editor = new TextArea("project_description",array("class"=>"ckeditor"));
//        $editor->addValidators(array(
//            new PresenceOf(array(
//                'Title' => 'The Title is required'
//            )),
//        ));

        // CSRF
        $csrf = new Hidden('csrf');
        $csrf->addValidator(new Identical(array(
             'value' => $this->security->getSessionToken(),
             'message' => 'CSRF validation failed'
        )));
        
        $this->add($title);
        $this->add($editor);
        $this->add($csrf);
        
        // Sign Up
        $this->add(new Submit('Add Project', array(
             'class' => 'btn btn-success'
        )));
    }
}