var hhcTasks;

(function($) {
    hhcTasks = {
        init: function() {
            CKEDITOR.replace('description');
            $("#add-task-form").submit(function() {
                $('textarea.ckeditor').each(function() {
                    var $textarea = $(this);
                    $textarea.val(CKEDITOR.instances[$textarea.attr('name')].getData());
                });
                hhcTasks.newTask(this);
                $("#tasks-list").sortable('refresh');
                $("#projects").mCustomScrollbar("update");
                return false;
            });
            /**
             * Sort Task orders
             */
            $("#tasks-list").sortable({
                placeholder: 'sortable-placeholder',
                items: '.item-box',
                handle: '.item-head',
                cursor: 'move',
                start: function(event, ui) {

                }
            });
            /** 
             * Delete Task Event
             */
            $(document).on("click", ".delete-task", function(e) {
                hhcTasks.deleteTask($(this).data("id"));
                $("#projects").mCustomScrollbar("update");
                return false;
            })
        },
        newTask: function(form) {
            var $form = $(form);
            var action = $form.attr("action")
            var form_data = $form.serializeObject();
            hhcAjax.postAjax(action, form_data, function(data) {
                $("#tasks-list").append(data);
                $form[0].reset();
                $('textarea.ckeditor').each(function() {
                    var $textarea = $(this);
                    CKEDITOR.instances[$textarea.attr('name')].setData();
                });
            }, function(data) {

            });
        },
        deleteTask: function(id) {
            hhcAjax.postAjax("tasks/delete", {id: id}, function(data) {
                if (data == "success") {
                    $("#task-" + id).fadeOut(200, function() {
                        $(this).remove();
                    });
                }
            });
        }
    }
    $(document).ready(function() {
        hhcTasks.init();
    });
})(jQuery);