var hhcAjax;
(function($) {
    $.fn.serializeObject = function() {
        var self = this,
                json = {},
                push_counters = {},
                patterns = {
                    "validate": /^[a-zA-Z][a-zA-Z0-9_]*(?:\[(?:\d*|[a-zA-Z0-9_]+)\])*$/,
                    "key": /[a-zA-Z0-9_]+|(?=\[\])/g,
                    "push": /^$/,
                    "fixed": /^\d+$/,
                    "named": /^[a-zA-Z0-9_]+$/
                };
        this.build = function(base, key, value) {
            base[key] = value;
            return base;
        };

        this.push_counter = function(key) {
            if (push_counters[key] === undefined) {
                push_counters[key] = 0;
            }
            return push_counters[key]++;
        };

        $.each($(this).serializeArray(), function() {

            // skip invalid keys
            if (!patterns.validate.test(this.name)) {
                return;
            }

            var k,
                    keys = this.name.match(patterns.key),
                    merge = this.value,
                    reverse_key = this.name;

            while ((k = keys.pop()) !== undefined) {

                // adjust reverse_key
                reverse_key = reverse_key.replace(new RegExp("\\[" + k + "\\]$"), '');

                // push
                if (k.match(patterns.push)) {
                    merge = self.build([], self.push_counter(reverse_key), merge);
                }
                else if (k.match(patterns.named) || k.match(patterns.fixed)) {
                    merge = self.build({}, k, merge);
                }
            }
            json = $.extend(true, json, merge);
        });
        return json;
    };
})(jQuery);
(function($) {
    hhcAjax = {
        init: function() {

        },
        postJsonAjax: function(url, data, done, fail) {

            var csrf = $("#csrf").attr("name");

            data[csrf] = $("#csrf").val();
            $.ajax({
                url: url,
                type: 'post',
                context: document.body,
                dataType: 'json',
                data: data
            }).done(function(data) {
                done(data);
            }).fail(function(data) {
                fail(data);
            });
        },
        postAjax: function(url, data, done, fail) {
            var csrf = $("#csrf").attr("name");
            data[csrf] = $("#csrf").val();
            $.ajax({
                url: url,
                type: 'post',
                context: document.body,
                data: data
            }).done(function(data) {
                done(data);
            }).fail(function(data) {
                fail(data);
            });
        },
        getAjax: function(url, data, done, fail) {
            $.ajax({
                url: url,
                type: 'get',
                context: document.body,
                data: data
            }).done(function(data) {
                done(data);
            }).fail(function(data) {
                fail(data);
            });
        }, showTab: function(a, success) {
            var target = a.attr("href"); // activated tab
            var url = a.data("url"); // tab action
            hhcAjax.appendLoader(target);
            hhcAjax.getAjax(url, {}, success);
        },
        appendLoader: function(target) {
            $(target).html('<div class="content-loader"><div class="loader-icon"><img src="/assets/img/loading.gif" /></div></div>');
        },
        removeLoader: function(target) {
            if ($(target).length)
                $(target).find(".content-loader").remove();
        }
    }
    $(document).ready(function() {
        hhcAjax.init();
    });
})(jQuery);