var hhcUsers;
(function($) {
    hhcUsers = {
        init: function() {
            $("#edit-profile").click(function() {
                $this = $(this);
                hhcAjax.getAjax($this.data('url'), {
                    id: $("#user-id").val()
                }, function(data) {
                    var target = $this.attr("href")
                    $(target).html(data);
                    $this.hide();
                });
                return false;
            });
            $(document).on('submit', '#save-profile', function() {
                hhcUsers.saveProfile($(this));
                return false;
            });
        },
        saveProfile: function(form) {
            var $form = $(form);
            var action = $form.attr("action")
            var form_data = $form.serializeObject();
            hhcAjax.postAjax(action, form_data, function(data) {
                $("#edit-me-wrap").html(data);
                $("#edit-profile").show();
            });
        },
        uploadAvatar: function(ele) {

            var clickedID = ele.attr("id");
            new AjaxUpload(clickedID, {
                action: ele.attr("href"),
                name: clickedID, // File upload name
                data: {// Additional data to send
                    action: 'pyre_upload',
                    type: 'upload',
                    data: clickedID},
                autoSubmit: true, // Submit file after selection
                responseType: false,
                onChange: function(file, extension) {
                },
                onSubmit: function(file, extension) {
                    this.disable(); // If you want to allow uploading only 1 file at time, you can disable upload button
                },
                onComplete: function(file, response) {
                    this.enable();
                    var res = $.parseJSON(response);
                    $("#user-avatar").attr("src", res.file_url);
                    $("#avatar_id").val(res.file_id);
                    // jQuery('.save_tip').fadeIn(400).delay(5000).fadeOut(400);
                }
            });

        }
    };
    $(document).ready(function() {
        hhcUsers.init();
    });
})(jQuery);