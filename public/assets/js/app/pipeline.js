  var hhcPipeline;
  (function($) {
     hhcPipeline = {
        init: function() {

        $sortable_columns = $(".sortable-column");
        $sortable_columns.sortable({
            placeholder: 'item-placeholder',
            items: '> .pipeline-item',
            
            cursor: 'move',
            distance: 2,
            containment: 'document',
            start: function(event, ui){},
            stop: function(event, ui){},
            activate: function(event, ui) {
               
            },
            over: function(e, ui) {
                 $(this).addClass('column-hover');
            },
            out: function(e, ui) { 
               $(this).removeClass('column-hover'); 
            },
            deactivate: function(event, ui) {
                 
            },
            receive: function(event, ui) {
                var $ele = ui.item;
                hhcPipeline.changeStage(
                            $ele.data("id"),
                            $(this).find("input.stage").val()
                        );
                
            }
         }).sortable('option', 'connectWith', 'div.sortable-column');
         
      $("#trash").droppable({
            accept: ".pipeline-item",
            drop: function(e, ui) {
            ui.draggable.fadeOut();
            ui.draggable.remove();
               console.log("drop");}
         });
         
        },
        changeStage: function(id, stage){
            hhcAjax.postAjax("/tasks/changestage",{
               id:id,
               stage:stage
            },function(data){
                console.log(data);
            },function(data){
                console.log(data);
            });
        }
     };

     $(document).ready(function() {
        hhcPipeline.init();
     });

  })(jQuery);
