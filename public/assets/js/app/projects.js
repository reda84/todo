 var hhcProjects;

  (function($) {
     hhcProjects = {
        init: function() {
            $("#add-project-form").submit(function(){
                $('textarea.ckeditor').each(function () {
                    var $textarea = $(this);
                    $textarea.val(CKEDITOR.instances[$textarea.attr('name')].getData());
                });
                hhcProjects.newProject(this);
                $("#projects-list").sortable('refresh');
                $("#projects").mCustomScrollbar("update");
                return false;
            });
            $("#projects-list").sortable({
              placeholder: 'sortable-placeholder',
              items: '.project',
              handle: '.project-head',
              cursor: 'move',
              start: function(event, ui) {
                  
              }
          });
          
          $(document).on("click",".delete-project",function(e){
              hhcProjects.deleteProject($(this).data("id"));
              $("#projects").mCustomScrollbar("update"); 
              return false;
          })
          
        },
        newProject: function(form){
            var $form = $(form);
            var action  = $form.attr("action")
            var form_data = $form.serializeObject();
            hhcAjax.postAjax(action, form_data , function(data) {
                $("#projects-list").append(data);
                $form[0].reset();
                 $('textarea.ckeditor').each(function() {
                    var $textarea = $(this);
                    console.log($textarea.attr('name'));
                    CKEDITOR.instances[$textarea.attr('name')].setData("");
                });
              }, function(data) {
                 
              });
        },
        deleteProject: function(id){
            hhcAjax.postAjax("projects/delete", { id:id } , function(data) {
                if(data == "success"){
                    $("#project-" + id).fadeOut(200,function(){
                        $(this).remove();
                    });    
                }
              });
        }
     }
     $(document).ready(function() {
        hhcProjects.init();
     });
  })(jQuery);